# Installation
Download the fartscroll.js library into your libraries folder, so that its
javascript file is available at "libraries/fartscroll.js/fartscroll.js".
https://github.com/theonion/fartscroll.js


Download and enable the Libraries API module and the Fartscroll module.
http://drupal.org/project/libraries
http://drupal.org/project/fartscroll


Give the desired roles the ability to administer website farting behavior at the
permissions page.


Administer farting behavior at Configuration >> System >> Farts.

Note: for users of Drush Make, an example is provided to automatically download
the library.
